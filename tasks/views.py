from django.shortcuts import render, redirect
from tasks.models import Task
from django.contrib.auth.decorators import login_required
from tasks.forms import CreateTask

# Create your views here.


@login_required
def task_create(request):
    if request.method == "POST":
        form = CreateTask(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    else:
        form = CreateTask()

    context = {
        "form": form,
    }
    return render(request, "tasks/create_task.html", context)


@login_required
def my_tasks(request):
    mine = Task.objects.filter(assignee=request.user)

    context = {
        "my_tasks": mine,
    }

    return render(request, "tasks/my_tasks.html", context)
