from django.shortcuts import render, get_object_or_404, redirect
from projects.models import Project
from django.contrib.auth.decorators import login_required
from projects.forms import CreateForm

# Create your views here.


@login_required
def project_list(request):
    project_list = Project.objects.filter(owner=request.user)
    context = {
        "project_list": project_list,
    }
    return render(request, "projects/home.html", context)


@login_required
def project_detail(request, id):
    detail = get_object_or_404(Project, id=id)
    context = {
        "detail_object": detail,
    }
    return render(request, "projects/detail.html", context)


@login_required
def create_project(request):
    if request.method == "POST":
        form = CreateForm(request.POST)
        if form.is_valid():
            name = form.save(False)
            name.owner = request.user
            name.save()
            return redirect("list_projects")
    else:
        form = CreateForm()

    context = {
        "form": form,
    }

    return render(request, "projects/create_project.html", context)
